//clang -I/usr/include/efi -I/usr/include/efi/x86_64 -I/usr/include/efi/protocol -fno-stack-protector -fpic -fshort-wchar -mno-red-zone -DHAVE_USE_MS_ABI -c -o src/main.o src/main.c
//ld -nostdlib -znocombreloc -T /usr/lib/elf_x86_64_efi.lds -shared -Bsymbolic -L /usr/lib /usr/lib/crt0-efi-x86_64.o src/main.o -o huehuehuehuehue.so -lefi -lgnuefi
//objcopy -j .text -j .sdata -j .data -j .dynamic -j .dynsym  -j .rel -j .rela -j .reloc --target=efi-app-x86_64 huehuehuehuehue.so huehuehuehuehue.efi

#include <efi.h>
#include <efilib.h>
#include <efiprot.h>

//#include "kernel/tty.h"

EFI_STATUS efi_main(EFI_HANDLE hImage, EFI_SYSTEM_TABLE *pSystemTable)
{
	EFI_STATUS status;
	
	// Boot stage
	
	InitializeLib(hImage, pSystemTable);
	
	EFI_MEMORY_DESCRIPTOR *pMemoryMap{LibMemoryMap(NULL, NULL, NULL, NULL)};
	
	// Exit boot stage and go into runtime
	pSystemTable->BootServices->ExitBootServices(hImage, boot_state.map_key);
	
	// Runtime stage
	
	//tty_init();
	
	Print(L"Hello, SORA!");
	
	// Shutdown
	
	pSystemTable->RuntimeServices->ResetSystem(EfiResetShutdown, EFI_SUCCESS, 0, NULL);
	
	while(true)
		__asm__("hlt");
};