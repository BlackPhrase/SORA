project(kernel)

enable_language(ASM-ATT)

add_executable(${PROJECT_NAME} main.cpp)

aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} PROJECT_SOURCES)

target_sources(${PROJECT_NAME}
	PRIVATE ${PROJECT_SOURCES}
)

target_compile_options(${PROJECT_NAME}
	PRIVATE -nostdlib -nostdinc -fno-builtin -fno-stack-protector
)